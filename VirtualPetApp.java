import java.util.Scanner;
public class VirtualPetApp {
    public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Fox[] skulk = new Fox[4];
		
		//(From here)	Only part that was added using ChatGPT
		//Used to clear the terminal
		System.out.print("\033[H\033[2J");
        System.out.flush();
		//(To here)
		
		System.out.println("Turn ur (imaginary..) friends into foxes!! :D");
		System.out.println("(u're prolly in in cs, ik u have no friends bruv :|)");
		
		for(int i=0; i<skulk.length; i++){
			System.out.println("\r\n"+ "--Fox " +(i+1)+ "--");
			System.out.println("Gimme the name of ur fox");
            String name = reader.nextLine();

            System.out.println("Gimme the habitat of ur fox");
            String habitat = reader.nextLine();

            System.out.println("Gimme the size of ur fox (in cm)");
            int size = reader.nextInt();
            reader.nextLine();

            System.out.println("Gimme a word to describe ur fox's vibe");
            String vibe = reader.nextLine();

            skulk[i] = new Fox(name, habitat, size, vibe);
		}
		
		System.out.println(skulk[skulk.length-1].animalCard());
		System.out.println(skulk[skulk.length-1].animalVibe());
    }
}