public class Fox{
	public String name;
	public String habitat;
	public int size;
	public String vibe;

	public Fox(String name, String habitat, int size, String vibe){
		this.name = name;
		this.habitat = habitat;
		this.size = size;
		this.vibe = vibe;
	}

	public String animalVibe(){
		return "This " +name+ " fox has a(n) "+vibe+ " vibe";
	}

	public String animalCard(){
		return "\r\n"+ "		[Random Fox Card]" +"\r\n"+ "Name: " +name+ "\r\n" + "Habitat: " +habitat+"\r\n"+"Size (in cm): " +size;
	}
}